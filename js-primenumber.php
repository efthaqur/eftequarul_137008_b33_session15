<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

<script language="JavaScript" type="text/javascript">

    for(num=1; num<=100;num++){
        if(num==1){
            continue; //if num is 1, loop will start form top again instead of going below
        }
        is_prime=true;
        for(i=2; i<=(num/2); i++){ //determine prime num or NOT!
            if(num%i==0){
                is_prime=false; //if remainder is 0 sets is_prime= falses
                break;
            }

        }
        if(is_prime==true){
            document.write("Prime Number is: "+num+"<br>");
        }
    }


</script>

</body>
</html>